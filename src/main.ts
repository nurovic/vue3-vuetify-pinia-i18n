import en from './locales/en.json'
import fr from './locales/fr.json'
import App from './App.vue'
import { createI18n } from 'vue-i18n'

const i18n = createI18n({
locale: "en",
messages: {
    en,
    fr
}
})

// Composables
import { createApp } from 'vue'

// Plugins
import { registerPlugins } from '@/plugins'

const app = createApp(App)

registerPlugins(app)

app.use(i18n)
app.mount('#app')
